/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heranca2;

import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {



    private ArrayList<Pessoa> listaP = new ArrayList();

    private ArrayList<Pessoa> listaA = new ArrayList();



    private Professor pr;

    private FuncAdm f;

    private Aluno a;



    private int i = 0;

    private int j;



    @FXML

    private Pane painel;



    @FXML

    private Button botaoDet, botDel, botAtua, enviar1, enviar2;



    @FXML

    private TextField txt1, txt2, txt3, txt4, txt5, txt6;



    @FXML

    private Label lblSalario, lblSemestre, lblCurso, lblSetor, lblFuncao, lblDisciplina;



    @FXML

    private RadioButton rbAluno, rbFunc, rbProf;



    @FXML

    private TableView<Pessoa> tabela;



    @FXML

    private TableColumn<Pessoa, String> nomeT;



    @FXML

    private TableColumn<Pessoa, Integer> idadeT;



    @FXML

    private TableColumn<Pessoa, String> enderecoT;



    @FXML

    private TableColumn<Pessoa, String> funcaoT;



    @FXML

    private TableColumn<Pessoa, String> semestreT;



    @FXML

    private TableColumn<Pessoa, String> cursoT;



    @FXML

    private TableColumn<Pessoa, Double> salarioT;



    @FXML

    private TableColumn<Pessoa, String> disciplinaT;



    @FXML

    private TableColumn<Pessoa, String> setorT;



    @FXML

    private TableColumn<Pessoa, String> funcao2T;



    @FXML

    final ToggleGroup group = new ToggleGroup();



    @Override

    public void initialize(URL url, ResourceBundle rb) {



        carregarDados();



        painel.setVisible(false);



        botaoDet.setVisible(false);

        botDel.setVisible(false);

        botAtua.setVisible(false);



        semestreT.setVisible(false);

        setorT.setVisible(false);

        disciplinaT.setVisible(false);

        salarioT.setVisible(false);

        cursoT.setVisible(false);

        funcaoT.setVisible(false);



        rbAluno.setToggleGroup(group);

        rbFunc.setToggleGroup(group);

        rbProf.setToggleGroup(group);



    }



    @FXML

    void showFormAluno() {

        painel.setVisible(true);



        txt6.setVisible(false);



        lblSemestre.setVisible(true);

        lblCurso.setVisible(true);

        lblDisciplina.setVisible(false);

        lblSalario.setVisible(false);

        lblSetor.setVisible(false);

        lblFuncao.setVisible(false);



        rbAluno.setVisible(false);

        rbProf.setVisible(false);

        rbFunc.setVisible(false);



        enviar1.setVisible(true);

        enviar2.setVisible(false);



    }



    @FXML

    void showFormFuncAdm() {

        painel.setVisible(true);



        txt6.setVisible(true);



        lblSalario.setVisible(true);

        lblSetor.setVisible(true);

        lblFuncao.setVisible(true);

        lblDisciplina.setVisible(false);

        lblCurso.setVisible(false);

        lblSemestre.setVisible(false);



        rbAluno.setVisible(false);

        rbProf.setVisible(false);

        rbFunc.setVisible(false);



        enviar1.setVisible(true);

        enviar2.setVisible(false);

    }



    @FXML

    void showFormProf() {

        painel.setVisible(true);



        txt6.setVisible(false);



        lblSalario.setVisible(true);

        lblDisciplina.setVisible(true);



        lblCurso.setVisible(false);

        lblSemestre.setVisible(false);

        lblSetor.setVisible(false);

        lblFuncao.setVisible(false);



        rbAluno.setVisible(false);

        rbProf.setVisible(false);

        rbFunc.setVisible(false);



        enviar1.setVisible(true);

        enviar2.setVisible(false);



    }



    @FXML

    void showBotao(MouseEvent event) {

        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {

            botaoDet.setVisible(true);

            botDel.setVisible(true);

            botAtua.setVisible(true);

        }

    }



    @FXML

    void criar(MouseEvent event) {



        if (lblDisciplina.isVisible()) {

            i = 3;

        } else if (lblCurso.isVisible()) {

            i = 1;

        } else if (lblSetor.isVisible()) {

            i = 2;

        }



        tabela.setItems(getListaDePessoas());



        lblSalario.setVisible(false);

        lblDisciplina.setVisible(false);

        lblCurso.setVisible(false);

        lblSemestre.setVisible(false);

        lblSetor.setVisible(false);

        lblFuncao.setVisible(false);



        txt6.setVisible(false);



        painel.setVisible(false);



        rbAluno.setVisible(true);

        rbProf.setVisible(true);

        rbFunc.setVisible(true);



    }



    @FXML

    void deletar(ActionEvent event) {



        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();

        selectedItem.deleta();

        tabela.getItems().remove(selectedItem);



        botDel.setVisible(false);

        botaoDet.setVisible(false);

        botAtua.setVisible(false);

        setorT.setVisible(false);

        salarioT.setVisible(false);

        disciplinaT.setVisible(false);

        cursoT.setVisible(false);

        semestreT.setVisible(false);

        funcaoT.setVisible(false);

    }



    @FXML

    void atualizar(ActionEvent event) {

        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();



        if ("Aluno".equals(selectedItem.getFuncao2())) {

            showFormAluno();

        }



        if ("Professor".equals(selectedItem.getFuncao2())) {

            showFormProf();

        }



        if ("FuncAdm".equals(selectedItem.getFuncao2())) {

            showFormFuncAdm();

        }



        enviar1.setVisible(false);

        enviar2.setVisible(true);



        botaoDet.setVisible(false);

        botDel.setVisible(false);

        botAtua.setVisible(false);



    }



    @FXML

    void atualizar2(MouseEvent event) {

        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();



        if ("Aluno".equals(selectedItem.getFuncao2())) {

            for (int x = 0; x < listaP.size(); x++) {

                if (listaP.get(x).getId() == selectedItem.getId()) {



                    Aluno al = new Aluno();

                    al.setId(selectedItem.getId());

                    al.setNome(txt1.getText());

                    al.setIdade(Integer.parseInt(txt2.getText()));

                    al.setEndereco(txt3.getText());

                    al.setCurso(txt5.getText());

                    al.setSemestre(txt4.getText());

                    al.setFuncao2("Aluno");



                    listaP.set(x, al);

                    selectedItem = listaP.get(x);

                    

                }

            }

        }else



        if ("Professor".equals(selectedItem.getFuncao2())) {

            for (int x = 0; x < listaP.size(); x++) {

                if (listaP.get(x).getId() == selectedItem.getId()) {



                    Professor b = new Professor();

                    b.setId(selectedItem.getId());

                    b.setNome(txt1.getText());

                    b.setIdade(Integer.parseInt(txt2.getText()));

                    b.setEndereco(txt3.getText());

                    b.setSalario(Double.parseDouble(txt4.getText()));

                    b.setDisciplina(txt5.getText());

                    b.setFuncao2("Professor");



                    listaP.set(x, b);

                    selectedItem = listaP.get(x);

                    

                    

                }

            }

        }else



        if ("FuncAdm".equals(selectedItem.getFuncao2())) {

            for (int x = 0; x < listaP.size(); x++) {

                if (listaP.get(x).getId() == selectedItem.getId()) {



                    FuncAdm c = new FuncAdm();

                    c.setId(selectedItem.getId());

                    c.setNome(txt1.getText());

                    c.setIdade(Integer.parseInt(txt2.getText()));

                    c.setEndereco(txt3.getText());

                    c.setSalario(Double.parseDouble(txt4.getText()));

                    c.setSetor(txt5.getText());

                    c.setFuncao(txt6.getText());

                    c.setFuncao2("FuncAdm");



                    listaP.set(x, c);

                    selectedItem = listaP.get(x);

                    



                }

            }

        }